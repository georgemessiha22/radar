USER_ID = 'USER_ID'
SRC_LANG_CODE = 'SRC_LANG_CODE'
DEST_LANG_CODE = 'DEST_LANG_CODE'
DEFAULT_SRC_LANG_CODE = 'en'
TEXT = 'TEXT'
SRC_TEXT = 'SRC_TEXT'
DEST_TEXT = 'DEST_TEXT'
TEXT_ID = 'TEXT_ID'

error_lang_not_available = {
	SRC_LANG_CODE: 'Source Language is not available.',
	DEST_LANG_CODE: 'Destination Language is not available.'
	}

# ID_TEXT_MAP consists of TEXT:TEXT_ID
TEXT_ID_MAP = {}

# DICTIONARY consists of 'SRC_LANG_CODE_DEST_LANG_CODE': { TEXT_ID_1: TEXT_ID_2}
DICTIONARY = {}


