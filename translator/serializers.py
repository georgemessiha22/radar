from googletrans import LANGUAGES
from rest_framework import serializers


class Translate_Request_Serializer(serializers.Serializer):
	SRC_LANG_CODE = serializers.ChoiceField(choices=LANGUAGES, allow_blank=True, allow_null=True)
	DEST_LANG_CODE = serializers.ChoiceField(choices=LANGUAGES, allow_null=False, allow_blank=False)
	TEXT = serializers.CharField(max_length=4096, min_length=1, allow_blank=False)


class Translate_Response_Serializer(serializers.Serializer):
	SRC_LANG_CODE = serializers.ChoiceField(choices=LANGUAGES, allow_blank=True, allow_null=True)
	DEST_LANG_CODE = serializers.ChoiceField(choices=LANGUAGES, allow_null=False, allow_blank=False)
	SRC_TEXT = serializers.CharField(max_length=4096, min_length=1, allow_blank=False)
	DEST_TEXT = serializers.CharField(max_length=4096, min_length=1, allow_blank=False)


class Translate_Response_Object(object):
	def __init__(self, SRC_LANG_CODE, DEST_LANG_CODE, SRC_TEXT, DEST_TEXT):
		self.SRC_LANG_CODE = SRC_LANG_CODE
		self.DEST_LANG_CODE = DEST_LANG_CODE
		self.SRC_TEXT = SRC_TEXT
		self.DEST_TEXT = DEST_TEXT
