from googletrans import Translator
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .constants import (SRC_LANG_CODE, DEST_LANG_CODE, DEFAULT_SRC_LANG_CODE, TEXT, TEXT_ID_MAP, DICTIONARY
                        )
from .serializers import Translate_Request_Serializer, Translate_Response_Serializer, Translate_Response_Object
from .utils import add_text_translation, generate_id


@api_view(['POST'])
def translate_request(request):
	'''
	Web Service that caches translations from Google translate to local service, for reuse.
	it count default SRC_LANG_CODE is 'en' if not exist.
	:param request: should have : { 'SRC_LANG_CODE' : 'en',
									'DEST_LANG_CODE': 'fr',
									'TEXT' : 'some text'
									}
									
	:return response:             { 'SRC_LANG_CODE' : 'en',
									'DEST_LANG_CODE': 'fr',
									'SRC_TEXT' : 'some text',
									'DEST_TEXT': 'translated text'
									}
			error: otherwise
	'''
	serializer = Translate_Request_Serializer(data=request.data)
	if not serializer.is_valid():
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
	request_data = serializer.validated_data
	src_lang = DEFAULT_SRC_LANG_CODE
	dest_lang = request_data[DEST_LANG_CODE]
	if SRC_LANG_CODE in request_data:
		if request_data[SRC_LANG_CODE]:
			src_lang = request_data[SRC_LANG_CODE]
	
	translation_code = src_lang + '_' + dest_lang
	rev_translation_code = dest_lang + '_' + src_lang
	if request_data[TEXT] not in TEXT_ID_MAP:
		TEXT_ID_MAP[request_data[TEXT]] = generate_id()
	
	if translation_code not in DICTIONARY:
		DICTIONARY[translation_code] = {}
	
	if rev_translation_code not in DICTIONARY:
		DICTIONARY[rev_translation_code] = {}
	
	if TEXT_ID_MAP[request_data[TEXT]] not in DICTIONARY[translation_code]:
		translator = Translator()
		translation = (translator.translate(request_data[TEXT], dest=dest_lang, src=src_lang)).text
		if translation not in TEXT_ID_MAP:
			TEXT_ID_MAP[translation] = generate_id()
	else:
		translation = list(TEXT_ID_MAP.keys())[
			list(TEXT_ID_MAP.values()).index(DICTIONARY[translation_code][TEXT_ID_MAP[request_data[TEXT]]])]
	
	if TEXT_ID_MAP[request_data[TEXT]] not in DICTIONARY[translation_code]:
		add_text_translation(src_lang=src_lang, dest_lang=dest_lang, text=request_data[TEXT], trans=translation)
	
	response = Translate_Response_Object(SRC_LANG_CODE=src_lang,
	                                     DEST_LANG_CODE=dest_lang,
	                                     SRC_TEXT=request_data[TEXT],
	                                     DEST_TEXT=translation)
	response = Translate_Response_Serializer(response)
	return Response(response.data)
