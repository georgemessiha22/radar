import uuid


class Text:
	
	def __init__(self, text):
		self.id = uuid.uuid4().hex
		self.text = text
		self.dictionary = {}
	
	def get_id(self):
		return self.id
	
	def add_translation(self, Text_object):
		self.dictionary[Text_object.get_id] = {Text_object}