import uuid
from .constants import DICTIONARY, TEXT_ID_MAP

def generate_id():
	'''
	Generate a unique id
	:return: hex unique id
	'''
	return uuid.uuid4().hex

def add_text_translation(src_lang, dest_lang, text, trans):
	'''
	It add new translation to the dictionary in the src_dest and dest_src to define the map.
	:param src_lang: Source language.
	:param dest_lang: Destination language.
	:param text: Text want to be translated.
	:param trans: Translation.
	:return:
	'''
	translation_code = src_lang + '_' + dest_lang
	if translation_code in DICTIONARY:
		DICTIONARY[translation_code][TEXT_ID_MAP[text]] = TEXT_ID_MAP[trans]
	else:
		DICTIONARY[translation_code] = {}
		DICTIONARY[translation_code][TEXT_ID_MAP[text]] = TEXT_ID_MAP[trans]
	
	# revert
	translation_code = dest_lang + '_' + src_lang
	if translation_code in DICTIONARY:
		DICTIONARY[translation_code][TEXT_ID_MAP[trans]] = TEXT_ID_MAP[text]
	else:
		DICTIONARY[translation_code] = {}
		DICTIONARY[translation_code][TEXT_ID_MAP[trans]] = TEXT_ID_MAP[text]