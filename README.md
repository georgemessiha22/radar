# TRANSLATING CACHING

This is a trial to build Translating cache architecture using Python Django Rest Framework and Google Translate API.

## Project Setup:

### Requirements:
 * Python 3.5
 * Pip
 * VirtualEnv

### Steps:
 * Initialize virtual env and activate it.
 * inside the folder directory run the following:
    ```Bash
    $  pip install -r requirements.txt
    ```

### Run:
 * to run server use the following in terminal with virtual env activated and inside the project directory:
    ```Bash
    $ python manage.py  runserver
    ```
 * to run shell use the following in terminal with virtual env activated and inside the project directory:
    ```Bash
    $ python manage.py  shell
    ```

## Further Implementation:
* Implements local DB using noSQL/SQL to attach user preferences of the language_1 and language_2 to predict what next translation user may use and fetch it.
* make routine tasks that clear the dict to be able to use minimal cache memory. creating a LOG of date_created and lastDateUsed values. and deciede to pop out of the dictionary depending on the lastDateUsed if available or date_created.
